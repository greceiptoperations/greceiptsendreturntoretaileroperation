﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DatabaseFirst
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class GReceiptKeyStoreEntities : DbContext
    {
        public GReceiptKeyStoreEntities()
            : base("name=GReceiptKeyStoreEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<ClientPreKeyBundle> ClientPreKeyBundles { get; set; }
        public virtual DbSet<UserAuth_Email> UserAuth_Email { get; set; }
        public virtual DbSet<EmailUserRegistration> EmailUserRegistration { get; set; }
        public virtual DbSet<UserAuth_Phone> UserAuth_Phone { get; set; }
        public virtual DbSet<GReceipt> GReceipts { get; set; }
        public virtual DbSet<ItemDetail> ItemDetails { get; set; }
        public virtual DbSet<Item> Items { get; set; }
        public virtual DbSet<Merchant> Merchants { get; set; }
        public virtual DbSet<MerchantUser> MerchantUsers { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<TransactionStatu> TransactionStatus { get; set; }
        public virtual DbSet<MerchantDetail> MerchantDetails { get; set; }
        public virtual DbSet<MerchantSetting> MerchantSettings { get; set; }
        public virtual DbSet<GReceiptsStatu> GReceiptsStatus { get; set; }
        public virtual DbSet<InterfaceDetail> InterfaceDetails { get; set; }
        public virtual DbSet<GReceiptsDigitalSignature> GReceiptsDigitalSignatures { get; set; }
    }
}
