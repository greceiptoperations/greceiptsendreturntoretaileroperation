﻿using System;
using System.Collections.Generic;


namespace GReceiptSendReturnsToRetailerOperation.Business
{

    public class AppSettings
    {
        public AWSConfiguration Section1 { get; set; }
        public GReceiptSenderConfiguration Section2 { get; set; }
    }


    public class AWSConfiguration
    {      
            
        public string Profile { get; set; }       
        public string Region { get; set; }
        public string AWSProfilesLocation { get; set; }
        
    }

    public class GReceiptSenderConfiguration
    {

        public string SendGReceiptDataTopic { get; set; }
        public string InterfaceName { get; set; }
        public string RootCAPath { get; set; }
        public string IotEndpoint { get; set; }
        public string Topic { get; set; }
    }

   
   
}
