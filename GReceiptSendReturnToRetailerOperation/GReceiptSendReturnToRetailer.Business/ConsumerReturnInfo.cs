﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GReceiptSendReturnsToRetailerOperation.Business
{
    public class ConsumerReturnInfo
    {
        public string consumerID  { get; set; }
        public string timestamp { get; set; }
        public string retailerID { get; set; }
        public string retailerName { get; set; }
        public string greceiptID { get; set; }
        public string greceiptData { get; set; }
        public ConsumerReturnItemInfo consumerReturnItemInfo { get; set; }

    }
}