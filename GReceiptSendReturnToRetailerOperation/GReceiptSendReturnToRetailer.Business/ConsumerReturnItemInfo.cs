﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GReceiptSendReturnsToRetailerOperation.Business
{
    public class ConsumerReturnItemInfo
    {              
        public string itemName{ get; set; }
        public string itemUPC { get; set; }
        public int totalQuantityOfItem { get; set; }
        public int quantityreturned { get; set; }
        public string returnStatus { get; set; }
       
    }
}