﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GReceiptSendReturnsToRetailerOperation.Business
{
    public class CustomersRatingInfo
    {
       
        public int ratingid { get; set; }
        public int retailerid { get; set; }
        public string label { get; set; }
        public int overallexperience { get; set; }
        public int consumerID { get; set; }
        public int ratingValue { get; set; }       
        public string itemname { get; set; }
        public string timestamp { get; set; }
        public int greceipt_id { get; set; }
        public DateTime createddate { get; set; }
        public string createdby { get; set; }
    }
}