﻿using System;
using System.Collections.Generic;

namespace GReceiptSendReturnsToRetailerOperation.Business
{
   
    public class GReceipt_Consumer
    {
       
        public int ConsumerID { get; set; }

      
        public string TimeStamp { get; set; }
        public string RetailerID { get; set; }
        public string RetailerName { get; set; }
        public string RetailerLogo { get; set; }
        public string RetailerAddress { get; set; }
        public string RetailerPublicKey { get; set; }
        public RetailerSettings RetailerSettings { get; set; }
        public SpecialSettings SpecialSettings { get; set; }
        public ICollection<RatingSettings> RatingSettings { get; set; }
        public System.Nullable<System.DateTime> order_date { get; set; }
        public string orderStatus { get; set; }
        public Nullable<decimal> subtotal { get; set; }
        public Nullable<decimal> tax { get; set; }
        public Nullable<decimal> shipping { get; set; }
        public Nullable<decimal> discounts { get; set; }
        public Nullable<decimal> total { get; set; }
        public string billing_name { get; set; }
        public string shippingAddress { get; set; }
        public string cc_brand { get; set; }
        public Nullable<int> cc_last_4 { get; set; }
        public Nullable<int> fulfillmentType_id { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }
        public ICollection<ItemDetailsDTO> ItemDetails { get; set; }
        public CustomersRatingInfo CustomerRatingInfo { get; set; }
        //public GReceiptsDataDTO GReceiptsData { get; set; }
        //public RetailerInfoDTO Retailer { get; set; }        

    }
}