﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GReceiptSendReturnsToRetailerOperation.Business
{
  
    public  class GReceiptsData
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public Nullable<System.DateTime> order_date { get; set; }
        public Nullable<int> transaction_id { get; set; }
        public Nullable<int> fulfillmentType_id { get; set; }
        public Nullable<int> orderStatus_id { get; set; }
        public Nullable<decimal> subtotal { get; set; }
        public Nullable<decimal> tax { get; set; }
        public Nullable<decimal> shipping { get; set; }
        public Nullable<decimal> discounts { get; set; }
        public Nullable<decimal> total { get; set; }
        public string billing_name { get; set; }
        public string shippingAddress { get; set; }
        public string cc_brand { get; set; }
        public Nullable<int> cc_last_4 { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }

        public int InterfaceID { get; set;}
    }
}
