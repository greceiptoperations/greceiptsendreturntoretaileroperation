﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GReceiptSendReturnsToRetailerOperation.Business
{
  
    public  class GReceiptsDigitalSignature
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string Signature { get; set; }
        public string certificatePem { get; set; }
        public string certificatePublicKey { get; set; }
        public Nullable<int> GReceipt_ID { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }
    }
}
