﻿using System;

namespace GReceiptSendReturnsToRetailerOperation.Business
{
  
    public  class GReceiptsDigitalSignatureDTO
    {      
        public string Signature { get; set; }
        public Nullable<int> GReceiptID { get; set; }
        public string certificatePem { get; set; }
        public string certificatePublicKey { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; }
    }
}
