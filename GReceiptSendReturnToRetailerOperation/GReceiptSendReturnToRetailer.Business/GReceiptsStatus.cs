﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GReceiptSendReturnsToRetailerOperation.Business
{

    public  class GReceiptsStatus
    {
        public int id { get; set; }
        public string status { get; set; }
        public System.DateTime created_date { get; set; }
        public System.DateTime updated_date { get; set; }
    }

}
