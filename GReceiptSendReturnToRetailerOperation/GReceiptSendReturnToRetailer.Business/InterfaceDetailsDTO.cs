﻿using System;

namespace GReceiptSendReturnsToRetailerOperation.Business
{
    public class InterfaceDetailsDTO
    {       
        public string InterfaceName { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; }
        public Nullable<System.DateTime> ModifiedDateTime { get; set; }
    }
}
