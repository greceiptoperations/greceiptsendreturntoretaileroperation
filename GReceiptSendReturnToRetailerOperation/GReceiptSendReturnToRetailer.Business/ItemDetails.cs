﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GReceiptSendReturnsToRetailerOperation.Business
{
    public class ItemDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public Nullable<int> GReceipt_id { get; set; }
        public Nullable<int> item_id { get; set; }
        public string itemname { get; set; }
        public string UPC { get; set; }
        public Nullable<decimal> itemPrice { get; set; }
        public Nullable<decimal> qty { get; set; }
        public Nullable<int> startedReturn { get; set; }
        public Nullable<int> returned { get; set; }
        public Nullable<int> exchanged { get; set; }
        public Nullable<int> exchanged_with { get; set; }

        public Nullable<int> merchant_id { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }
    }
}
