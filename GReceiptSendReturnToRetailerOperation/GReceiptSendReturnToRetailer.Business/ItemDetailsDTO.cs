﻿using System;


namespace GReceiptSendReturnsToRetailerOperation.Business
{
    public class ItemDetailsDTO
    {
        public string itemname { get; set; }
        public string UPC { get; set; }
        public Nullable<decimal> itemPrice { get; set; }
        public Nullable<decimal> qty { get; set; }
        public Nullable<int> startedReturn { get; set; }
        public Nullable<int> returned { get; set; }
        public Nullable<int> exchanged { get; set; }
        public Nullable<int> exchanged_with { get; set; }
        public Nullable<int> merchant_id { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }
    }
}
