﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GReceiptSendReturnsToRetailerOperation.Business
{
    public class MerchantSettings
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int merchant_id { get; set; }
        public string merchant_checkout_url { get; set; }
        public Nullable<int> enable_returns { get; set; }
        public Nullable<int> return_require_go_ahead { get; set; }
        public Nullable<int> return_require_reason { get; set; }
        public Nullable<int> return_require_time_limit { get; set; }
        public Nullable<int> return_calculate_based_percent { get; set; }
        public Nullable<int> return_allow_reinit { get; set; }
        public Nullable<int> return_allow_multiple_items { get; set; }
        public Nullable<int> return_require_manager_approval { get; set; }
        public Nullable<int> specials_enable { get; set; }
        public Nullable<int> specials_item_level { get; set; }
        public Nullable<int> specials_brand_level { get; set; }
        public Nullable<int> ratings_enable { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }

    }
}
