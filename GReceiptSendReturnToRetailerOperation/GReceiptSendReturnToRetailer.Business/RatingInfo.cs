﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GReceiptSendReturnsToRetailerOperation.Business
{
    public class RatingInfo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ratinginfo_id { get; set; }
        public Nullable<int> ratingid { get; set; }
        public Nullable<int> merchant_id { get; set; }
        public Nullable<int> greceipt_id { get; set; }
        public Nullable<int> item_id { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }
       
    }
}