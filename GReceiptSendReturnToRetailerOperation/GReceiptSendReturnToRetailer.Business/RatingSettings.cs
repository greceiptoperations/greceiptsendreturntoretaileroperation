﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GReceiptSendReturnsToRetailerOperation.Business
{
    public class RatingSettings
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ratingID { get; set; }
        public int merchant_id { get; set; }
        public string label { get; set; }
        public int active { get; set; }
        public int overallexperience { get; set; }
    }
}