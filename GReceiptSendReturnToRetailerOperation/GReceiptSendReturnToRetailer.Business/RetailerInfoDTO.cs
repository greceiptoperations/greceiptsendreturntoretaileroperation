﻿using System.Collections.Generic;

namespace GReceiptSendReturnsToRetailerOperation.Business
{
    public class RetailerInfoDTO
    {
        public string RetailerID { get; set; }
        public string RetailerName { get; set; }
        public string RetailerLogo { get; set; }
        public string RetailerAddress { get; set; }
        public string RetailerPublicKey { get; set; }
        public RetailerSettings RetailerSettings { get; set; }
        public SpecialSettings SpecialSettings { get; set; }
        public ICollection<RatingSettings> RatingSettings { get; set; }
        public GReceiptsDataDTO GReceiptsData { get; set; }

    }
}