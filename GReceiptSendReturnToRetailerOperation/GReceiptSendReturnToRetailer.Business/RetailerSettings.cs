﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GReceiptSendReturnsToRetailerOperation.Business
{
    public class RetailerSettings
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int merchant_id { get; set; }
        public int EnableReturn { get; set; }
        public int EnableSpecials { get; set; }
        public int EnableRatings { get; set; }
        public System.Nullable<System.DateTime> updated_date { get; set; }
        public string created_by { get; set; }
        public string Updated_by { get; set; }
    }
}