﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GReceiptSendReturnsToRetailerOperation.Business
{
    public class SpecialSettings
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int specialID { get; set; }
        public int merchant_id { get; set; }
        public string SpecialsEnabled { get; set; }
        public string ItemAdvertisement { get; set; }                        

    }
}