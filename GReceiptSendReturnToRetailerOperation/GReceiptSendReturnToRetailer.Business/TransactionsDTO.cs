﻿using System;
using System.Collections.Generic;

namespace GReceiptSendReturnsToRetailerOperation.Business{
    
    public class TransactionsDTO
    {
        public string name { get; set; }
        public string merchantOrderID { get; set; }
        public string RetailerLogo { get; set; }
        public Nullable<int> ConsumerID { get; set; }
        public string transactionStatus { get; set; }
        public InterfaceDetailsDTO InterfaceDetails { get; set; }
        public GReceiptsDataDTO GReceiptsData { get; set; }
        public GReceiptsDigitalSignatureDTO GReceiptsDigitalSignature { get; set; }
        public RetailerSettings retailerSettings { get; set; }
        public SpecialSettings specialSettings { get; set; }
        public ICollection<RatingSettings> ratingSettings { get; set; }
    }
}
