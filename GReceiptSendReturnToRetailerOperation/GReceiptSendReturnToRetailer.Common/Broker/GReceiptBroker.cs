﻿
using System;
using System.Collections.Generic;
using System.Text;
using Amazon.IoT.Model;
using Amazon.Runtime.CredentialManagement;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Org.BouncyCastle.Asn1.X509;
using System.Collections;
using System.IO;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Exceptions;
using GReceiptInterfaceReceiver.Common.Helpers;
using GReceiptSendReturnsToRetailerOperation.Business;
using GReceiptSendReturnsToRetailerOperation.Common.Enums;

namespace GReceiptSendReturnsToRetailerOperation.Common.Broker
{
    public class GReceiptBroker
    {
        IConfiguration Configuration ;
        bool isConnected = false;
        CreateCertificateFromCsrResponse ocert = null;
        CreateThingResponse ocreatethingresponse = null;
        AttachThingPrincipalRequest oAttachThingPrincipalRequest = null;
        AttachThingPrincipalResponse oAttachThingPrincipalResponse = null;
        AttachPolicyRequest oAttachPolicyRequest = null;
        AttachPolicyResponse oAttachPolicyResponse = null;
        
        string pfxPath;
        string certPath;

      
        public async Task<bool> SendRatingInfoToRetailer(CustomersRatingInfo ratingInfo)
        {
            var builder = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json");

            Configuration = builder.Build();

            bool IsDataSentSuccessful = false;
            //Retreive AWS Credentials from .ini file
            var options = Configuration.GetAWSOptions();
            var chain = new CredentialProfileStoreChain(options.ProfilesLocation);
            Amazon.Runtime.AWSCredentials awsCredentials;

            var path = new Uri(
               System.IO.Path.GetDirectoryName(
                   System.Reflection.Assembly.GetExecutingAssembly().CodeBase)
               ).LocalPath;

            if (chain.TryGetAWSCredentials(options.Profile, out awsCredentials))
            {
                // Use awsCredentials

            }

            //Initialize AWS IoTClient
            var iotClient = new Amazon.IoT.AmazonIoTClient(awsCredentials, options.Region);

            //Get the configuration values from appsettings.json file
            IConfiguration config = Configuration.GetSection("GReceiptSendReturnsToRetailerOperation");
            string policyName = config["SendRatingDataToRetailer"];
            string interfaceName = config["InterfaceName"];
            string rootCAPath = config["RootCAPath"];
            string IotEndpoint = config["IotEndpoint"];
            string Topic = config["Topic"];
            string CSRCountryName = config["CSRCountryName"];
            string CSRDomainName = config["CSRDomainName"];
            string CSRCompanyName = config["CSRCompanyName"];


            //If Certificate, private key and pfx file already exists, we dont need to re-generate them.
            if (!File.Exists(path + @"\" + interfaceName + "Cert" + ".cer"))
            {

                //Create a Thing specific to Retailer Interface
                CreateThingRequest ocreatethingrequest = new CreateThingRequest();
                ocreatethingrequest.ThingName = interfaceName + "Thing";
                ocreatethingresponse = await iotClient.CreateThingAsync(ocreatethingrequest);

                //Generate Elliptical Curve Public/PrivateKey Pair
                //Demo Subject DNs
                IDictionary subjectDN = new Hashtable();
                subjectDN.Add(X509Name.C, CSRCountryName);
                subjectDN.Add(X509Name.CN, CSRDomainName);
                subjectDN.Add(X509Name.O, CSRCompanyName);

                Org.BouncyCastle.X509.X509CertificateParser parser =
                    new Org.BouncyCastle.X509.X509CertificateParser();

                var oclientCert = new GReceiptSendReturnsToRetailerOperation.Common.CAService.PublicKeyBase();

                //Generate CSR using the details of the retailer.Using Public key + subject -> to create CSR signed by private key
                string csr = oclientCert.GenerateKeyPairAndCSR(subjectDN);



                if (!string.IsNullOrEmpty(csr))
                {


                    //Create a Certificate
                    // The path to the certificate.
                    //ocert = await iotClient.CreateKeysAndCertificateAsync(true);
                    ocert = await iotClient.CreateCertificateFromCsrAsync(csr, true);


                    //Attach a Thing to Certificate
                    oAttachThingPrincipalRequest = new AttachThingPrincipalRequest();
                    oAttachThingPrincipalRequest.ThingName = ocreatethingrequest.ThingName;
                    oAttachThingPrincipalRequest.Principal = ocert.CertificateArn;
                    oAttachThingPrincipalResponse = await iotClient.AttachThingPrincipalAsync(oAttachThingPrincipalRequest);


                    //Attach a policy to certificate
                    oAttachPolicyRequest = new AttachPolicyRequest();
                    oAttachPolicyRequest.PolicyName = policyName;
                    oAttachPolicyRequest.Target = ocert.CertificateArn;
                    oAttachPolicyResponse = await iotClient.AttachPolicyAsync(oAttachPolicyRequest);


                    certPath = path + @"\" + interfaceName + "Cert" + ".cer";

                    //Get the bytes of client certificate
                    var cert = DigitalSignatureUtility.GetBytesFromPEM(ocert.CertificatePem, PemStringType.Certificate);
                    System.IO.MemoryStream pkcs12Stream = new System.IO.MemoryStream();

                    var x509cert = parser.ReadCertificate(cert);
                    //Write the certificate to a file path
                    System.IO.File.WriteAllText(certPath, ocert.CertificatePem);

                    //For PKCS12 <aka PFX> you can include CA chain or not.
                    oclientCert.CreatePKCS12(pkcs12Stream, "ganesh12345#",
                                            parser.ReadCertificate(cert),
                                             null);
                    pfxPath = path + @"\" + interfaceName + "pfx" + ".pfx";
                    System.IO.File.WriteAllBytes(pfxPath, pkcs12Stream.ToArray());
                }


            }


            pfxPath = path + @"\" + interfaceName + "pfx" + ".pfx";
            //Client Certificate
            var clientCert = new System.Security.Cryptography.X509Certificates.X509Certificate2(pfxPath, "ganesh12345#");
                      

            //This is not needed. Only for testing .Verify the consumer received the same message sent by the sender using the public key of the retailer.
            //oganesh.Receive(hashBytes,signature,clientCert);

            //Certificate provided by Amazon. Signed by Symantec CA. CA Cert and Client certificate are required to establish
            //mutual authentication between Retailer Interface and AWS IoT
            var caCert = System.Security.Cryptography.X509Certificates.X509Certificate.CreateFromCertFile(rootCAPath);

            // Initialize the MQTT client
            var client = new MqttClient(IotEndpoint, MqttClientSettings.MQTT_BROKER_DEFAULT_SSL_PORT, true, caCert, clientCert, MqttSslProtocols.TLSv1_2);

            //Convert the JSON to string
            string message = JsonConvert.SerializeObject(ratingInfo);
            //message to publish - could be anything                 


            string clientId = Guid.NewGuid().ToString();
            //client naming has to be unique if there was more than one publisher
            client.Connect(clientId);
            //publish to the topic
            client.Publish(Topic, Encoding.UTF8.GetBytes(message));
            //this was in for debug purposes but it's useful to see something in the console
            if (client.IsConnected)
                isConnected = true;
            return IsDataSentSuccessful;
        }
    }
}
