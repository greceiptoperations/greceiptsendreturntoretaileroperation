﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Asn1.Pkcs;
using Org.BouncyCastle.Crypto;
using System.IO;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.X509;
using Org.BouncyCastle.Asn1.X9;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Asn1.X509;
using System.Collections;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Crypto.Operators;
//using Org.BouncyCastle.Utilities.Encoders;
using Org.BouncyCastle.X509.Extension;


namespace GReceiptSendReturnsToRetailerOperation.Common.CAService
{
    public enum KeyType
    {
        RSA = 1,
        EC = 2
    }
    public class Curve
    {
        public const string RSA = "RSA";
        public const string CURVE_P256 = "P-256";
        public const string CURVE_P384 = "P-384";
        public const string CURVE_P521 = "P-521";
        public const string CURVE_P224 = "P-224";
        public const string CURVE_SECP256R1 = "secp256r1";
        public const string CURVE_SECP384R1 = "secp384r1";
        public const string CURVE_SECP521R1 = "secp521r1";
    }
    public class PublicKeyBaseException: Exception
    {
        public const string ERROR_MESSAGE_UNKNOWN_PUBLIC_KEY_ALGO   = "Unknown public key algorithm.";
        public const string ERROR_MESSAGE_KEYPAIR_ALREADY_GENERATED = "The keypair already generated.";
        public PublicKeyBaseException(string message)
            : base(message)
        {
        }

        public PublicKeyBaseException(string message, Exception e)
            : base(message, e)
        {
        }
    }
    public class PublicKeyBase
    {
        Pkcs10CertificationRequest _pkcs10  = null;
        AsymmetricCipherKeyPair _keyPair    = null;
        KeyType _keyType                    = KeyType.EC;
        string _keyName                     = Curve.CURVE_P256;
        SubjectKeyIdentifierStructure _subjectKeyIdentifier = null;
        public PublicKeyBase()
        {

        }
        void GenerateKeyPair()
        {
            if (_keyPair != null)
                throw new PublicKeyBaseException(PublicKeyBaseException.ERROR_MESSAGE_KEYPAIR_ALREADY_GENERATED);

            if (string.IsNullOrEmpty(_keyName))
                throw new PublicKeyBaseException(PublicKeyBaseException.ERROR_MESSAGE_UNKNOWN_PUBLIC_KEY_ALGO);


            switch (_keyType)
            {
                case KeyType.EC:
                    string curveName = null;
                    switch (_keyName)
                    {
                        case Curve.CURVE_P256:
                        case Curve.CURVE_SECP256R1:
                            curveName = Curve.CURVE_SECP256R1;
                            break;

                        case Curve.CURVE_P384:
                        case Curve.CURVE_SECP384R1:
                            curveName = Curve.CURVE_SECP384R1;
                            break;

                        case Curve.CURVE_P521:
                        case Curve.CURVE_SECP521R1:
                            curveName = Curve.CURVE_SECP521R1;
                            break;
                        default:
                            curveName = _keyName;
                            break;
                    }

                    if (string.IsNullOrEmpty(curveName))
                        return;

                    X9ECParameters curve = ECNamedCurveTable.GetByName(curveName);
                    ECDomainParameters ecDomainParams = new ECDomainParameters(curve.Curve, curve.G, curve.N, curve.H);
                    ECKeyPairGenerator kpg = new ECKeyPairGenerator("ECDSA");
                    kpg.Init(new KeyGenerationParameters(new SecureRandom(), ecDomainParams.Curve.FieldSize));
                    _keyPair = kpg.GenerateKeyPair();
                    _subjectKeyIdentifier = new SubjectKeyIdentifierStructure(_keyPair.Public);
                    return;
                case KeyType.RSA:
                    RsaKeyPairGenerator rkpg = new RsaKeyPairGenerator();
                    rkpg.Init(new KeyGenerationParameters(new SecureRandom(), 2048));
                    _keyPair = rkpg.GenerateKeyPair();
                    _subjectKeyIdentifier = new SubjectKeyIdentifierStructure(_keyPair.Public);
                    return;
                default:
                    return;
            }
        }
        X509Name GenerateX509Subject(IDictionary subjectDN)
        {
            return new X509Name(new ArrayList(subjectDN.Keys), subjectDN);
        }
        public string GenerateKeyPairAndCSR(IDictionary subjectDN, KeyType keyType = KeyType.EC, string keyName = Curve.CURVE_P256)
        {
            _keyType = keyType;
            _keyName = keyName;

            X509Name subject2GenerateCSR = GenerateX509Subject(subjectDN); //new X509Name(new ArrayList(attrs.Keys), attrs);
            GenerateKeyPair();
            Asn1Set attributes = null;
            Asn1SignatureFactory signatureFactory = null;
            if (_keyType == KeyType.RSA)
            {
                signatureFactory = new Asn1SignatureFactory("SHA256withRSA", _keyPair.Private);
                _pkcs10 = new Pkcs10CertificationRequest
                    (signatureFactory,
                  subject2GenerateCSR,
                  _keyPair.Public,
                  attributes,
                  _keyPair.Private);
            }
            else
            {
                signatureFactory = new Asn1SignatureFactory("SHA256withECDSA", _keyPair.Private);
                switch (_keyName)
                {

                    case Curve.CURVE_P384:
                    case Curve.CURVE_SECP384R1:
                        signatureFactory = new Asn1SignatureFactory("SHA384withECDSA", _keyPair.Private);
                        break;

                    case Curve.CURVE_P521:
                    case Curve.CURVE_SECP521R1:
                        signatureFactory = new Asn1SignatureFactory("SHA512withECDSA", _keyPair.Private);
                        break;
                }

                _pkcs10 = new Pkcs10CertificationRequest
                    (signatureFactory,
                  subject2GenerateCSR,
                  _keyPair.Public,
                  attributes,
                  _keyPair.Private);
            }
            return Convert.ToBase64String(_pkcs10.GetEncoded());
        }
        public void CreatePKCS12( Stream stream, string password,
                                    X509Certificate cert, 
                                    X509Certificate[] chain = null)
        {
            Pkcs12Store p12 = new Pkcs12Store();
            AsymmetricKeyEntry keyEntry = new AsymmetricKeyEntry(_keyPair.Private);
            int n = 1;
            if (chain != null)
                n += chain.Length;

            X509CertificateEntry[] certEntryChain = new X509CertificateEntry[n];
            X509CertificateEntry certEntry = new X509CertificateEntry(cert);
            certEntryChain[0] = certEntry;
            if (chain != null)
            {
                int i = 1;
                foreach (X509Certificate c in chain)
                {
                    var ce = new X509CertificateEntry(c);
                    certEntryChain[i] = ce;
                    i++;
                }
            }

            p12.SetKeyEntry("", keyEntry, certEntryChain);
            p12.Save(stream, password.ToCharArray(),
                        new Org.BouncyCastle.Security.SecureRandom());

        }
    }
}
