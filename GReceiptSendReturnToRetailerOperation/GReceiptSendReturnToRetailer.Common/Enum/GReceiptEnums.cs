﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GReceiptSendReturnsToRetailerOperation.Common.Enums
{
    /// <summary>
    /// Helper methods for Enums
    /// </summary>   
    /// <author>
    /// Siva Manickam (smanickam)
    /// </author>
    /// <date>
    /// 03/05/2015 11:39:58 A
    /// </date>
    /// <remarks>
    /// 
    /// </remarks>
    /// <summary>
    /// Helper methods for Enums
    /// </summary>   
    /// <author>
    /// Siva Manickam (smanickam)
    /// </author>
    /// <date>
    /// 03/05/2015 11:39:58 A
    /// </date>
    /// <remarks>
    /// 
    /// </remarks>
    public enum PemStringType
    {
        ECCPublickey,
        ECCPrivateKey,
        Certificate,
        RsaPrivateKey
    }

    public enum TransactionStatusEnum
    {
        Complete = 1,
        InProcess = 2,
        Cancelled = 3,
        CartComplete = 4
    }

    public enum GReceiptStatusEnum
    {
        PreFulfillment = 1,
        PostFullfillment = 2
    }
}
