
using System;

namespace GReceiptSendReturnsToRetailerOperation.Common
{
    /// <summary>
    /// Timeout on receiving from broker exception
    /// </summary>
    public class MqttTimeoutException : Exception
    {
    }
}