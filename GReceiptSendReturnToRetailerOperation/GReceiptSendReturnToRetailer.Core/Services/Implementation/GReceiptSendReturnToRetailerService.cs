﻿using System;
using System.Collections.Generic;
using System.Text;
using Amazon.IoT.Model;
using Amazon.Runtime.CredentialManagement;
using Microsoft.Extensions.Configuration;
using System.IO;
using Amazon.Lambda.Core;
using System.Threading.Tasks;
using GReceiptSendReturnsToRetailerOperation.Business;
using GReceiptSendReturnsToRetailerOperation.Core.Services.Interface;
using GReceiptSendReturnsToRetailerOperation.DataAccess.Implementation;
using GReceiptSendReturnsToRetailerOperation.DataAccess.Interface;

namespace GReceiptSendReturnsToRetailerOperation.Core.Services.Implementation
{
    public class GReceiptSendReturnToRetailerService : IGReceiptSendReturnToRetailerService
    {
        //Receiver consumer return information and send them to retailer database
        async Task<bool> IGReceiptSendReturnToRetailerService.SendReturnDataToRetailer(List<ConsumerReturnInfo> consumerReturnInfoList, IConfiguration configuration)
        {
            bool isInsertSuccessful = false;

            try
            {
                LambdaLogger.Log("In Interface Core method ReceiveGReceiptData");
                IGReceiptReturnDataToRetailerRepository oRepo = new GReceiptReturnDataToRetailerRepository();
                isInsertSuccessful = await oRepo.SendGReceiptReturnDataToRetailer(consumerReturnInfoList);
                return isInsertSuccessful;
            }
            catch (Exception ex)
            {
                LambdaLogger.Log("Inner Exception on database repo SendGReceiptReturnDataToRetailer() for retailer :" + consumerReturnInfoList[0].retailerName + "and retailerID" + consumerReturnInfoList[0].retailerID + "Inner Exception : " + ex.InnerException.ToString());
                LambdaLogger.Log("Stack Trace on database repo SendGReceiptReturnDataToRetailer() for retailer :" + consumerReturnInfoList[0].retailerName + "and retailerID" + consumerReturnInfoList[0].retailerID + "Stack Trace : " + ex.StackTrace.ToString());
                LambdaLogger.Log("Source on database repo SendGReceiptReturnDataToRetailer() for retailer :" + consumerReturnInfoList[0].retailerName + "and retailerID" + consumerReturnInfoList[0].retailerID + "Stack Trace : " + ex.Source.ToString());
            }
            return true;
        }
    }

       
 }

