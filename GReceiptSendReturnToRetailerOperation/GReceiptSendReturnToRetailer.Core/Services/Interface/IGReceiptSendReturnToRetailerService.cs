﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GReceiptInterfaceReceiver.Business;
using GReceiptSendReturnsToRetailerOperation.Business;
using Microsoft.Extensions.Configuration;

namespace GReceiptSendReturnsToRetailerOperation.Core.Services.Interface
{
    public interface IGReceiptSendReturnToRetailerService
    {
         Task<bool> SendReturnDataToRetailer(List<ConsumerReturnInfo> consumerReturnInfoList, IConfiguration configuration);
        
    }

}
