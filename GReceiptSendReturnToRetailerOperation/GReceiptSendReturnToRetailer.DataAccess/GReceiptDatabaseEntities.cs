﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GReceiptSendReturnsToRetailerOperation.Business;
using Microsoft.Extensions.Configuration;
using System.IO;
namespace GReceiptSendReturnsToRetailerOperation.DataAccess
{

    /// <remarks>
    /// 
    /// </remarks>
    public class GReceiptEntities
    {
        IConfiguration Configuration;
        public string connectionstring {get;set;}
        public string database { get; set; }
        public GReceiptEntities()
        {
            var builder = new ConfigurationBuilder()
                      .SetBasePath(Directory.GetCurrentDirectory())
                      .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            connectionstring = Configuration.GetSection("MongoConnection:ConnectionString").Value; //"mongodb+srv://PayDatumAdmin:PayDatum2017@klubkangaroocluster-h6rte.mongodb.net/greceipts_retailer_klubKangaroo?retryWrites=true";
            database = Configuration.GetSection("MongoConnection:Database").Value;
        }
    }

   
}



