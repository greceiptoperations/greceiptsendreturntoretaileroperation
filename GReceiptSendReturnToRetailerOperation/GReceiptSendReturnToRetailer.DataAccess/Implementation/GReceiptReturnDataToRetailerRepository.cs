﻿using GReceiptSendReturnsToRetailerOperation.DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using GReceiptSendReturnsToRetailerOperation.Business;
using GReceiptSendReturnsToRetailerOperation.Common.Enums;
using System.Linq;
using Amazon.Lambda.Core;
using GReceiptSendReturnsToRetailerOperation.Common.Broker;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Amazon.IoT.Model;
using Amazon.Runtime.CredentialManagement;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using System.IO;
using MongoDB.Driver;

namespace GReceiptSendReturnsToRetailerOperation.DataAccess.Implementation
{
   public class GReceiptReturnDataToRetailerRepository : IGReceiptReturnDataToRetailerRepository
    {
        async Task<bool> IGReceiptReturnDataToRetailerRepository.SendGReceiptReturnDataToRetailer(List<ConsumerReturnInfo> consumerReturnInfoList)
        {
            bool isInsertSuccessful = true;
            
            LambdaLogger.Log("Inside IGReceiptRatingDataToRetailerRepository");           
            
            try
            {                
                GReceiptEntities oDatabaseEntities = new GReceiptEntities();
                string greceiptconsumerreturnInfo = "greceipt_consumerreturninfo";
                var client = new MongoClient(oDatabaseEntities.connectionstring);
                var db = client.GetDatabase(oDatabaseEntities.database);

                //Insert Customers rating information to retailer database
                var GReceiptDataCollection = db.GetCollection<ConsumerReturnInfo>(greceiptconsumerreturnInfo);
                foreach(ConsumerReturnInfo consumerReturnInfo in consumerReturnInfoList)
                {
                    await GReceiptDataCollection.InsertOneAsync(consumerReturnInfo);
                }

                isInsertSuccessful = true;
            }
                catch (Exception ex)
                {
                    LambdaLogger.Log("Inner Exception on database repo SendGReceiptReturnDataToRetailer() for retailer :" + consumerReturnInfoList[0].retailerName + "and retailerID" + consumerReturnInfoList[0].retailerID + "Inner Exception : " + ex.InnerException.ToString());
                    LambdaLogger.Log("Stack Trace on database repo SendGReceiptReturnDataToRetailer() for retailer :" + consumerReturnInfoList[0].retailerName + "and retailerID" + consumerReturnInfoList[0].retailerID + "Stack Trace : " + ex.StackTrace.ToString());
                    LambdaLogger.Log("Source on database repo SendGReceiptReturnDataToRetailer() for retailer :" + consumerReturnInfoList[0].retailerName + "and retailerID" + consumerReturnInfoList[0].retailerID + "Stack Trace : " + ex.Source.ToString());
                             
                    isInsertSuccessful = false;
               
                }
            return isInsertSuccessful;
        }

        
    }
}
