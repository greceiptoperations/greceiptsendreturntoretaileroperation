﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GReceiptSendReturnsToRetailerOperation.Business;

namespace GReceiptSendReturnsToRetailerOperation.DataAccess.Interface
{
    public interface IGReceiptReturnDataToRetailerRepository
    {
         Task<bool> SendGReceiptReturnDataToRetailer(List<ConsumerReturnInfo> consumerReturnInfoList);
    }
}
