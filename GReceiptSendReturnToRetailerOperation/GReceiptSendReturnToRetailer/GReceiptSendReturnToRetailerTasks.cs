using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Amazon.Lambda.Core;
using GReceiptSendReturnsToRetailerOperation.Business;
using Microsoft.Extensions.Configuration;
using System.IO;
using GReceiptSendReturnsToRetailerOperation.Core.Services.Implementation;
using GReceiptSendReturnsToRetailerOperation.Core.Services.Interface;
/**
* @author Siva Manickam
*
* @date - 11/24/2018
*/
/// <summary>
///  GReceipt Send Return to Retailer Operation is the program that receives consumers return data through MQTT broker to
///  and inserts return data into retailer database. 
///  Assumptions : For every receipt Printer in retailer store, there will be an equivalent GReceipt Virtual Printer program (GReceipt Interface Sender program)
///  Assumptions : For every retailer online store, there will be one Receipt Virtual Printer program (GReceipt Interface Sender program) for the entire site.
///  Modified code to use MongoDB

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace GReceiptSendReturnsToRetailerOperation
{
    public class GReceiptSendReturnToRetailerTasks
    {
        /// <summary>
        /// Default constructor that Lambda will invoke.
        /// </summary>
        /// 
        private readonly IGReceiptSendReturnToRetailerService _service;

        //public IConfiguration Configuration;

        bool isInsertSuccessful = false;
        public IConfiguration Configuration { get; set; }
        public GReceiptSendReturnToRetailerTasks()
        {
             _service = new GReceiptSendReturnToRetailerService();
        }

        public async Task<bool> GReceiptSendReturnsToRetailerOperation(List<ConsumerReturnInfo>  consumerReturnInfoList, ILambdaContext context)
        {
            try
            {
                if (consumerReturnInfoList != null && consumerReturnInfoList.Count > 0)
                {                   
                    LambdaLogger.Log("Inside method GReceiptSendReturnsToRetailerOperation");
                    isInsertSuccessful = await _service.SendReturnDataToRetailer(consumerReturnInfoList, Configuration);
                    if (isInsertSuccessful)
                        LambdaLogger.Log("GReceipt Consumer Return Data inserted into retailer database");
                    else
                        LambdaLogger.Log("Failed to insert consumer return data to retailer database");

                }
            }
            catch(Exception ex)
            {
                LambdaLogger.Log("Inner Exception on GReceiptSendReturnsToRetailerOperation for retailer :" + consumerReturnInfoList[0].retailerName + "and retailerID" + consumerReturnInfoList[0].retailerID + "Inner Exception : " + ex.InnerException.ToString());
                LambdaLogger.Log("Stack Trace on GReceiptSendReturnsToRetailerOperation for retailer :" + consumerReturnInfoList[0].retailerName + "and retailerID" + consumerReturnInfoList[0].retailerID + "Stack Trace : " + ex.StackTrace.ToString());
                LambdaLogger.Log("Source on GReceiptSendReturnsToRetailerOperation for retailer :" + consumerReturnInfoList[0].retailerName + "and retailerID" + consumerReturnInfoList[0].retailerID + "Stack Trace : " + ex.Source.ToString());
            }
            return isInsertSuccessful;
        }

        
    }
}
