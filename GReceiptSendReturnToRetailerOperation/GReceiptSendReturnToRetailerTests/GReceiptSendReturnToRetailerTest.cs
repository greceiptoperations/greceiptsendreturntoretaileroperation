using Amazon.Lambda.TestUtilities;
using GReceiptSendReturnsToRetailerOperation;
using GReceiptSendReturnsToRetailerOperation.Business;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using Xunit;

namespace GReceiptSendRatingToRetailerTests
{
    public class GReceiptSendReturnToRetailerTest
    {
        [Fact]
        public async Task<bool> GReceiptSendReturnDataToRetailer()
        {
            TestLambdaContext context = new TestLambdaContext();
            GReceiptSendReturnToRetailerTasks functions = new GReceiptSendReturnToRetailerTasks();
            ConsumerReturnInfo consumerReturnInfo = new ConsumerReturnInfo();
            ConsumerReturnItemInfo consumerReturnItemInfo = new ConsumerReturnItemInfo();
            ConsumerReturnItemInfo returnList = new ConsumerReturnItemInfo();
            List<ConsumerReturnInfo> consumerReturnInfoList = new List<ConsumerReturnInfo>();

            consumerReturnInfo.consumerID = "c8e1c467-91c6-4afe-ae7c-76b0a0d0bfb2";
            consumerReturnInfo.greceiptID = "5bdd2c88df7e2536c870552e";
            consumerReturnInfo.retailerID = "5bb4396c1c9d44000046554b";
            consumerReturnInfo.retailerName = "KlubKangaroo";
            consumerReturnItemInfo.itemName = "Samsung HDMICable";
            consumerReturnItemInfo.itemUPC = "75774884";
            consumerReturnItemInfo.quantityreturned = 2;
            consumerReturnItemInfo.totalQuantityOfItem = 3;
            consumerReturnItemInfo.returnStatus = "Pending";
            consumerReturnInfo.consumerReturnItemInfo = consumerReturnItemInfo;
            consumerReturnInfo.timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
                                            CultureInfo.InvariantCulture);
            consumerReturnInfo.greceiptData = "  {\r\n      \"order_date\": \"2017-03-03 18:36:57\",    \r\n      \"subtotal\": \"30.00\",\r\n      \"shipping\": \"4.00\",\r\n      \"tax\": \"5.40\",\r\n      \"discounts\": \"0.00\",\r\n      \"total\": \"39.40\",\r\n      \"billing_name\": \"AJ Ganesh\",\r\n      \"shippingAddress\": \"35 seconf Street, Moscow, PA, 18444\r\n\",     \r\n      \"ItemDetails\": [\r\n        {\r\n          \"itemname\": \"Benj Moore Painty\",\r\n          \"UPC\": \"12726600\",\r\n          \"itemPrice\": \"15.00\",\r\n          \"qty\": \"1.00\",\r\n          \"startedReturn\": null,\r\n          \"returned\": null,\r\n          \"exchanged\": null\r\n        },\r\n        {\r\n          \"itemname\": \"ganesh food Paint\",\r\n          \"UPC\": \"12726601\",\r\n          \"itemPrice\": \"17.00\",\r\n          \"qty\": \"3.00\",\r\n          \"startedReturn\": null,\r\n          \"returned\": null,\r\n          \"exchanged\": null\r\n        }\r\n      ]\r\n   }";
            consumerReturnInfoList.Add(consumerReturnInfo);

            ConsumerReturnItemInfo consumerReturnItemInfo1 = new ConsumerReturnItemInfo();
            ConsumerReturnInfo consumerReturnInfo1 = new ConsumerReturnInfo();

            consumerReturnInfo1.consumerID = "c8e1c467-91c6-4afe-ae7c-76b0a0d0bfb2";
            consumerReturnInfo1.greceiptID = "5bdd2c88df7e2536c870552e";
            consumerReturnInfo1.retailerID = "5bb4396c1c9d44000046554b";
            consumerReturnInfo1.retailerName = "KlubKangaroo";
            consumerReturnItemInfo1.itemName = "Benj Moore Painty";
            consumerReturnItemInfo1.itemUPC = "5576878";
            consumerReturnItemInfo1.quantityreturned = 1;
            consumerReturnItemInfo1.totalQuantityOfItem = 1;
            consumerReturnItemInfo.returnStatus = "Pending";
            consumerReturnInfo1.consumerReturnItemInfo = consumerReturnItemInfo1;
            consumerReturnInfo1.timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
                                            CultureInfo.InvariantCulture);
            consumerReturnInfo1.greceiptData = "  {\r\n      \"order_date\": \"2017-03-03 18:36:57\",    \r\n      \"subtotal\": \"30.00\",\r\n      \"shipping\": \"4.00\",\r\n      \"tax\": \"5.40\",\r\n      \"discounts\": \"0.00\",\r\n      \"total\": \"39.40\",\r\n      \"billing_name\": \"AJ Ganesh\",\r\n      \"shippingAddress\": \"35 seconf Street, Moscow, PA, 18444\r\n\",     \r\n      \"ItemDetails\": [\r\n        {\r\n          \"itemname\": \"Benj Moore Paintyy\",\r\n          \"UPC\": \"12726600\",\r\n          \"itemPrice\": \"15.00\",\r\n          \"qty\": \"1.00\",\r\n          \"startedReturn\": null,\r\n          \"returned\": null,\r\n          \"exchanged\": null\r\n        },\r\n        {\r\n          \"itemname\": \"ganesh food Paint\",\r\n          \"UPC\": \"12726601\",\r\n          \"itemPrice\": \"17.00\",\r\n          \"qty\": \"3.00\",\r\n          \"startedReturn\": null,\r\n          \"returned\": null,\r\n          \"exchanged\": null\r\n        }\r\n      ]\r\n   }";
            //  string tempdata = "GReceiptData\":\"  {\\r\\n      \\\"order_date\\\": \\\"2017-03-03 18:36:57\\\",    \\r\\n      \\\"subtotal\\\": \\\"30.00\\\",\\r\\n      \\\"shipping\\\": \\\"4.00\\\",\\r\\n      \\\"tax\\\": \\\"5.40\\
            consumerReturnInfoList.Add(consumerReturnInfo1);
            bool isInsertSuccessful = await functions.GReceiptSendReturnsToRetailerOperation(consumerReturnInfoList, context);
            Assert.True(isInsertSuccessful);
            return isInsertSuccessful;
            
            //Assert.Equal("Hello MyStepFunctions", state.Message);
        }
    }
}
